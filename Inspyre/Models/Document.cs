﻿using System;

namespace Inspyre.Models
{
	public class Document
	{
		public string FileName { get; set; }

		public int WordCount { get; set; }

		public string PlainText { get; set; }
	}
}
