﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using Inspyre.Utils;

namespace Inspyre.Controls
{
	public partial class MarkupTextBox
	{
		public string GetText( FlowDocument document )
		{
			return new TextRange( document.ContentStart, document.ContentEnd ).Text;
		}

		public MarkupTextBox()
		{
			InitializeComponent();
		}

		public string PlainText
		{
			get { return (string) GetValue( PlainTextProperty ); }
			set { SetValue( PlainTextProperty, value ); }
		}

		protected override void OnTextInput( TextCompositionEventArgs e )
		{
			Reformat();
			base.OnTextInput( e );
		}

		string TXT => new TextRange( Document.ContentStart, Document.ContentEnd ).Text;

		private void Reformat()
		{
			/*IEnumerable<IFormattedTextSegment> segments = MarkupUtils.InterpretMarkup( TXT );
			foreach ( IFormattedTextSegment segment in segments )
			{
			}*/
		}

		protected override void OnTextChanged( TextChangedEventArgs e )
		{
			/*InvalidateProperty( PlainTextProperty );
			string text = e.NewValue as string ?? string.Empty;
			string[] pieces = Regex.Split( text, MarkupDetectionRegex );

			Paragraph currentBlock = new Paragraph();
			Run currentRun = new Run();
			currentBlock.Inlines.Add( currentRun );
			Document.Blocks.Clear();
			Document.Blocks.Add( currentBlock );

			MarkupFormatFlags currentFormatting = MarkupFormatFlags.None;
			StringBuilder runTextBuilder = new StringBuilder();

			for ( int index = 0; index < pieces.Length; ++index )
			{
				if ( pieces[index].Length == 1 && pieces[index][0] == '*' )
				{
					// Modify our currentFormatting so we know what formatting to apply to this next Run
					bool isBoldingAsterisk = ( index + 1 < pieces.Length && pieces[index + 1].Length == 1 && pieces[index + 1][0] == '*' );
					if ( isBoldingAsterisk )
					{
						if ( currentFormatting.HasFlag( MarkupFormatFlags.Bold ) )
						{
							currentFormatting &= ~MarkupFormatFlags.Bold;
						}
						else
						{
							currentFormatting |= MarkupFormatFlags.Bold;
						}
					}
					else
					{
						if ( currentFormatting.HasFlag( MarkupFormatFlags.Italics ) )
						{
							currentFormatting &= ~MarkupFormatFlags.Italics;
						}
						else
						{
							currentFormatting |= MarkupFormatFlags.Italics;
						}
					}

					currentRun.Text = runTextBuilder.ToString();

					currentRun = new Run();
					currentBlock.Inlines.Add( currentRun );
					if ( currentFormatting.HasFlag( MarkupFormatFlags.Italics ) )
					{
						currentRun.FontStyle = FontStyles.Italic;
					}
					if ( currentFormatting.HasFlag( MarkupFormatFlags.Bold ) )
					{
						currentRun.FontWeight = FontWeights.Bold;
					}
				}

				runTextBuilder.Append( pieces[index] );
			}
			*/
			base.OnTextChanged( e );
		}

		public static readonly DependencyProperty PlainTextProperty = DependencyProperty.Register( "PlainText", typeof( string ), typeof( MarkupTextBox ) );
	}
}
