﻿using System;
using System.Windows;
using PubSub;
using Inspyre.Models;

namespace Inspyre.ViewModels
{
	public sealed class TextEditorViewModel : DependencyObject
	{
		public TextEditorViewModel()
		{
			this.Subscribe<DocumentOpenedEvent>( OnDocumentOpened );


			// TESTING
			this.Publish<DocumentOpenedEvent>( new DocumentOpenedEvent( new Document() ) );
		}

		public bool HasDocumentOpen
		{
			get { return (bool) GetValue( HasDocumentOpenProperty ); }
			private set { SetValue( HasDocumentOpenProperty, value ); }
		}

		public string DocumentPlainText
		{
			get { return (string) GetValue( DocumentPlainTextProperty ); }
			set { SetValue( DocumentPlainTextProperty, value ); }
		}

		private void OnDocumentOpened( DocumentOpenedEvent e )
		{
			_currentDocument = e.Document;
			HasDocumentOpen = true;
		}

		private static void OnDocumentPlainTextChanged( DependencyObject o, DependencyPropertyChangedEventArgs e )
		{
			TextEditorViewModel viewModel = (TextEditorViewModel) o;
			if ( !viewModel.HasDocumentOpen )
			{
				return;
			}

			viewModel._currentDocument.PlainText = (string) e.NewValue ?? string.Empty;
		}

		public static readonly DependencyProperty HasDocumentOpenProperty = DependencyProperty.Register( "HasDocumentOpen",
			typeof( bool ), typeof( TextEditorViewModel ) );
		public static readonly DependencyProperty DocumentPlainTextProperty = DependencyProperty.Register( "DocumentPlainText",
			typeof( string ), typeof( TextEditorViewModel ), new FrameworkPropertyMetadata( OnDocumentPlainTextChanged ) );

		private Document _currentDocument;
	}
}
