﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inspyre.Utils
{
	public static class MarkupUtils
	{
		private class FormattedTextSegment : IFormattedTextSegment
		{


			public string Text { get; set; }

			public bool IsBold { get; set; }

			public bool IsItalicized { get; set; }

			public override string ToString()
			{
				return $"{Text} (bold: {IsBold}, italics: {IsItalicized})";
			}
		}

		[Flags]
		private enum MarkupFormatFlags
		{
			None = 0,
			Italics = 1,
			Bold = 2
		}


		public static IEnumerable<IFormattedTextSegment> InterpretMarkup( string text )
		{
			if ( string.IsNullOrEmpty( text ) )
			{
				yield break;
			}

			int currentIndex = 0;
			int indexOfNextAsterisk = text.IndexOf( "*", StringComparison.InvariantCulture );
			MarkupFormatFlags currentFormat = MarkupFormatFlags.None;
			while ( indexOfNextAsterisk >= 0 )
			{
				MarkupFormatFlags previousFormat = currentFormat;

				int additionalCharactersToIncludeInPreviousText = 0;
				int currentIndexOffset = 1;
				bool isBold = ( indexOfNextAsterisk + 1 < text.Length && text[indexOfNextAsterisk + 1] == '*' );
				if ( isBold )
				{
					++currentIndexOffset;
					if ( ( currentFormat & MarkupFormatFlags.Bold ) == MarkupFormatFlags.Bold )
					{
						currentFormat &= ~MarkupFormatFlags.Bold;
						additionalCharactersToIncludeInPreviousText = 2;
					}
					else
					{
						currentFormat |= MarkupFormatFlags.Bold;
					}
				}
				else
				{
					if ( ( currentFormat & MarkupFormatFlags.Italics ) == MarkupFormatFlags.Italics )
					{
						currentFormat &= ~MarkupFormatFlags.Italics;
						additionalCharactersToIncludeInPreviousText = 1;
					}
					else
					{
						currentFormat |= MarkupFormatFlags.Italics;
					}
				}

				yield return new FormattedTextSegment
				{
					Text = text.Substring( currentIndex, indexOfNextAsterisk - currentIndex + additionalCharactersToIncludeInPreviousText ),
					IsBold = previousFormat.HasFlag( MarkupFormatFlags.Bold ),
					IsItalicized = previousFormat.HasFlag( MarkupFormatFlags.Italics )
				};

				currentIndex = indexOfNextAsterisk + additionalCharactersToIncludeInPreviousText;
				indexOfNextAsterisk = text.IndexOf( "*", currentIndex + currentIndexOffset, StringComparison.InvariantCultureIgnoreCase );
			}

			if ( currentIndex != text.Length )
			{
				yield return new FormattedTextSegment
				{
					Text = text.Substring( currentIndex, text.Length - currentIndex ),
					IsBold = currentFormat.HasFlag( MarkupFormatFlags.Bold ),
					IsItalicized = currentFormat.HasFlag( MarkupFormatFlags.Italics )
				};
			}
		}
	}
}
