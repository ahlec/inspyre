﻿using System;

namespace Inspyre.Utils
{
	public interface IFormattedTextSegment
	{
		string Text { get; }

		bool IsBold { get; }

		bool IsItalicized { get; }
	}
}
