﻿using System;
using Inspyre.Models;

namespace Inspyre
{
	public class DocumentOpenedEvent
	{
		public DocumentOpenedEvent( Document newlyOpenedDocument )
		{
			Document = newlyOpenedDocument;
		}

		public Document Document { get; }
	}
}
